
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

//'https://rest.ehrscape.com/rest/v1/view/91d9a698-7fed-4d4d-9b33-1ae7cf5ebae9

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
	return "Basic " + btoa(username + ":" + password);
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

/* global L, distance */

var mapa;

const LJ_LAT = 46.056946;
const LJ_LNG = 14.505752;

window.addEventListener('load', function () {
	
	if(!(document.URL.indexOf("nacrt.html") >= 0) && !(document.URL.indexOf("opis.html") >= 0) && !(document.URL.indexOf("bolnisnice.html") >= 0)) {
		generate();
	}
	
	if(document.URL.indexOf("bolnisnice.html") >= 0) { 
	
		// Osnovne lastnosti mape
		var mapOptions = {
			center: [LJ_LAT, LJ_LNG],
			zoom: 13
			// maxZoom: 3
		};
	
		// Ustvarimo objekt mapa
		mapa = new L.map('mapa', mapOptions);
	
		// Ustvarimo prikazni sloj mape
		var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
	
		// Prikazni sloj dodamo na mapo
		mapa.addLayer(layer);
		
		//Ob kliku na mapo, se pobarvajo najblizje bolnisnice
		mapa.on('click', function(ev) {
			var latlng = mapa.mouseEventToLatLng(ev.originalEvent);
			izrisiBolnisnice(latlng.lat, latlng.lng);
			
			
		})
		
		izrisiBolnisnice(LJ_LAT, LJ_LNG);
	}
	
});



function pridobiPodatke(callback) {

	var xobj = new XMLHttpRequest();
	xobj.overrideMimeType("application/json");
	xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
	xobj.onreadystatechange = function () {
		// rezultat ob uspešno prebrani datoteki
		if (xobj.readyState == 4 && xobj.status == "200") {
				var json = JSON.parse(xobj.responseText);
				
				// vrnemo rezultat
				callback(json);
		}
	};
	xobj.send(null);
}

var polygons = [];
var markers = [];

function izrisiBolnisnice(latS, lngS) {
	
	for(var i in polygons) {
		mapa.removeLayer(polygons[i]);
	}
	
	for(var i in markers) {
		mapa.removeLayer(markers[i]);
	}
	
	pridobiPodatke(function(json) {
		
		for(var i=0; i < json.features.length; i++) {
			
			var bolnisnica = json.features[i];
			var opis = [];
			
			//Opis bolnišnice
			opis.ime = bolnisnica.properties.name;
			opis.ulica = bolnisnica.properties['addr:street'];
			opis.stUlice = bolnisnica.properties['addr:housenumber'];
			opis.mesto = bolnisnica.properties['addr:city'];
			
			zamenjajKoordinate(bolnisnica);
			
			if(bolnisnica.geometry.type == "Point") {
				
				var bol = L.latLng(bolnisnica.geometry.coordinates);
				var razdalja = distance(latS, lngS, bol.lat, bol.lng, "K");
				
				if(razdalja < 4) {
					dodajMarker(bol.lat, bol.lng, opis, "green");
				} else {
					dodajMarker(bol.lat, bol.lng, opis, "red");
				}
				
			}else if(bolnisnica.geometry.type == "LineString") {
				
				var bol = L.latLng(bolnisnica.geometry.coordinates[0]);
				var razdalja = distance(latS, lngS, bol.lat, bol.lng, "K");
				
				if(razdalja < 4) {
					var polygon = L.polygon([bolnisnica.geometry.coordinates], {fillColor: 'green', color: 'green', opacity: 1});
					polygon.bindPopup('<p>' + (opis.ime == undefined ? "" : opis.ime + '</br>') + (opis.ulica == undefined ? '' : (opis.ulica + ' '))
											+ (opis.stUlice == undefined ? '' : (opis.stUlice + ', ')) + (opis.mesto == undefined ? '' : opis.mesto) + '</p>');
					polygon.addTo(mapa);
					polygons.push(polygon);
				} else {
					var polygon = L.polygon([bolnisnica.geometry.coordinates], {fillColor: 'blue', color: 'blue', opacity: 1});
					polygon.bindPopup('<p>' + (opis.ime == undefined ? "" : opis.ime + '</br>') + (opis.ulica == undefined ? '' : (opis.ulica + ' '))
											+ (opis.stUlice == undefined ? '' : (opis.stUlice + ', ')) + (opis.mesto == undefined ? '' : opis.mesto) + '</p>');
					polygon.addTo(mapa);
					polygons.push(polygon);
				}
					
			}else {
				
				for(var j = 0; j < bolnisnica.geometry.coordinates.length; j++) { 
					var bol = L.latLng(bolnisnica.geometry.coordinates[j][0]);
					var razdalja = distance(latS, lngS, bol.lat, bol.lng, "K");
					
					if(razdalja < 4) {
						var polygon = L.polygon([bolnisnica.geometry.coordinates[j]], {fillColor: 'green', color: 'green', opacity: 1});
						polygon.bindPopup('<p>' + (opis.ime == undefined ? "" : opis.ime + '</br>') + (opis.ulica == undefined ? '' : (opis.ulica + ' '))
											+ (opis.stUlice == undefined ? '' : (opis.stUlice + ', ')) + (opis.mesto == undefined ? '' : opis.mesto) + '</p>');
						polygon.addTo(mapa);
						polygons.push(polygon);
					} else {
						var polygon = L.polygon([bolnisnica.geometry.coordinates[j]], {fillColor: 'blue', color: 'blue', opacity: 1});
						polygon.bindPopup('<p>' + (opis.ime == undefined ? "" : opis.ime + '</br>') + (opis.ulica == undefined ? '' : (opis.ulica + ' '))
											+ (opis.stUlice == undefined ? '' : (opis.stUlice + ', ')) + (opis.mesto == undefined ? '' : opis.mesto) + '</p>');
						polygon.addTo(mapa);
						polygons.push(polygon);
					}
				}
			}
			
		}
	});
}

function zamenjajKoordinate(bolnisnica) {
	var lat, long;
	if(bolnisnica.geometry.type == "Point") {
		lat = bolnisnica.geometry.coordinates[1];
		long = bolnisnica.geometry.coordinates[0];
		bolnisnica.geometry.coordinates[0] = lat;
		bolnisnica.geometry.coordinates[1] = long;
	} else if(bolnisnica.geometry.type == "LineString"){
	
			for(var i = 0; i < bolnisnica.geometry.coordinates.length; i++) {
				lat = bolnisnica.geometry.coordinates[i][1];
				long = bolnisnica.geometry.coordinates[i][0];
				bolnisnica.geometry.coordinates[i][0] = lat;
				bolnisnica.geometry.coordinates[i][1] = long;
				
			}
	
	} else if(bolnisnica.geometry.type == "Polygon"){
			
			for(var i = 0; i < bolnisnica.geometry.coordinates.length; i++) {
				for(var j = 0; j < bolnisnica.geometry.coordinates[i].length; j++) {
					lat = bolnisnica.geometry.coordinates[i][j][1];
					long = bolnisnica.geometry.coordinates[i][j][0];
					bolnisnica.geometry.coordinates[i][j][0] = lat;
					bolnisnica.geometry.coordinates[i][j][1] = long;
				}
			}
	}
}

function dodajMarker(lat, lng, opis, tip) {
  var ikona = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-icon-2x-' + 
      (tip == 'green' ? 'green' : 'blue') + 
      '.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-shadow.png',
    iconSize: [20, 32.8],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [32.8, 32.8]
  });

  // Ustvarimo marker z vhodnima podatkoma koordinat 
  // in barvo ikone, glede na tip
  var marker = L.marker([lat, lng], {icon: ikona});
  markers.push(marker);

  // Izpišemo želeno sporočilo v oblaček
  marker.bindPopup('<p>' + (opis.ime == undefined ? "" : opis.ime + '</br>') + (opis.ulica == undefined ? '' : (opis.ulica + ' '))
						+ (opis.stUlice == undefined ? '' : (opis.stUlice + ', ')) + (opis.mesto == undefined ? '' : opis.mesto) + '</p>');

  // Dodamo točko na mapo
  marker.addTo(mapa);
}

//var patient1Id, patient2Id, patient3Id;
var currentId;

function kreirajEHRzaBolnika(ime, priimek, datumRojstva, callback) {

		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
            	callback(ehrId, partyData);
            	
            }
          },
          error: function(err) {
          	console.log("Napaka");
          }
        });
      }
		});
}

function generate() {
	//Vsakic, ko zgeneriramo podatke, na novo ustvarimo spustni meni
	var myNode = document.getElementById("dropdownMeni");
	while (myNode.firstChild) {
	    myNode.removeChild(myNode.firstChild);
	}
	
	//
	myNode = document.getElementById("infoEhr");
	while (myNode.firstChild) {
	    myNode.removeChild(myNode.firstChild);
	}
	
	pocistiNaStrani();
  	
	
	for(var i = 1; i <=3; i++) {
		generirajPodatke(i);
	}
}

function pocistiNaStrani() {
	//Podatke o pacientu nastavimo na prazna
	$("#name").html("<b>Ime:</b> ");
  	$("#surname").html("<b>Priimek:</b> ");
  	$("#date").html("<b>Datum rojstva:</b> ");
  	$("#dateAndTime").html("");
  	if(bmiChart != undefined) {
	  	bmiChart.clear();
	  	krvniTlakChart.clear();
  	}
  	$("#panelBmi").html("");
  	$("#panelKrvni").html("");
  	$("#dateOfMeasure").html("");
  	desnoPodatkiPocisti();
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 */
function generirajPodatke(stPacienta) {
	
	//v spustnem meniju se prikazejo 3 vzorčni pacienti
	if(stPacienta == 1) {
		
		//ustvarimo spustni meni
		$("#dropdownMeni").append(
				"<button class='btn btn-primary btn-lg dropdown-toggle' type='button' id='izbiraPacienta' data-toggle='dropdown'>Izbira pacienta<span class='caret'></span></button><ul class='dropdown-menu dropdown-menu-center' id='spustniMeni'></ul>"
		);
		$("#spustniMeni").append(
				"<li><a href='#' id='patOne'>1.pacient</a></li> \
				<li><a href='#' id='patTwo'>2.pacient</a></li> \
				<li><a href='#' id='patThree'>3.pacient</a></li>"
		);
		kreirajEHRzaBolnika("Domen", "Vilar", "1938-10-30", function(ehrid, opis) {
			$("#infoEhr").append("<div class='alert alert-success alertEhr' role='alert'>" + ehrid + "</div>");
			dodajMeritveVitalnihZnakov(ehrid, "2016-06-09T23:40Z", 179, 110, 37, 105, 85);
			dodajMeritveVitalnihZnakov(ehrid, "2017-07-08T11:40Z", 178, 100, 37, 120, 80);
			dodajMeritveVitalnihZnakov(ehrid, "2018-08-09T23:40Z", 179, 95, 37, 115, 100);
			dodajMeritveVitalnihZnakov(ehrid, "2019-05-09T23:40Z", 179, 90, 37, 115, 100);
			dodajMeritveVitalnihZnakov(ehrid, "2019-05-12T23:40Z", 179, 85, 37, 115, 100);
			dodajMeritveVitalnihZnakov(ehrid, "2020-05-09T23:40Z", 179, 80, 37, 110, 80);
			
			$('#patOne').click(function(){
			    pocistiNaStrani();
			    preberiEHRodBolnika(ehrid);
			    currentId=ehrid;
			    
			    //preberemo vse vitalne znake
			});
		});
		
	} else if(stPacienta == 2) {
		kreirajEHRzaBolnika("John", "Sturgis", "1999-10-30", function(ehrid, opis) {
			$("#infoEhr").append("<div class='alert alert-success alertEhr' role='alert'>" + ehrid + "</div>");
			dodajMeritveVitalnihZnakov(ehrid, "2014-05-09T23:40Z", 192, 86, 37, 100, 80);
			dodajMeritveVitalnihZnakov(ehrid, "2015-05-08T11:40Z", 190, 90, 37, 120, 80);
			dodajMeritveVitalnihZnakov(ehrid, "2016-05-09T23:40Z", 191, 90, 37, 110, 100);
			dodajMeritveVitalnihZnakov(ehrid, "2018-05-09T23:40Z", 192, 85, 37, 112, 80);
			dodajMeritveVitalnihZnakov(ehrid, "2019-05-09T23:40Z", 192, 84, 37, 123, 90);
			
			
			$('#patTwo').click(function(){
				 pocistiNaStrani();
			    preberiEHRodBolnika(ehrid);
			    currentId = ehrid;
			});
			
		});
	} else {
		kreirajEHRzaBolnika("Bruce", "Lee", "2000-10-30", function(ehrid, opis) {
			$("#infoEhr").append("<div class='alert alert-success alertEhr' role='alert'>" + ehrid + "</div>");
			dodajMeritveVitalnihZnakov(ehrid, "2013-05-09T23:40Z", 170, 70, 37, 155, 113);
			dodajMeritveVitalnihZnakov(ehrid, "2015-05-08T11:40Z", 170, 60, 37, 140, 90);
			dodajMeritveVitalnihZnakov(ehrid, "2016-05-09T23:40Z", 170, 63, 37, 150, 100);
			dodajMeritveVitalnihZnakov(ehrid, "2018-05-09T23:40Z", 170, 80, 37, 160, 105);
			dodajMeritveVitalnihZnakov(ehrid, "2019-05-09T23:40Z", 170, 85, 37, 115, 80);
			
			$('#patThree').click(function(){
				 pocistiNaStrani();
			    preberiEHRodBolnika(ehrid);
			    currentId = ehrid;
			});
		});
	}
	
	//prikazi idje dodanih pacientov
}

/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom in merilec).
 */
function dodajMeritveVitalnihZnakov(ehrId, datumInUra, telesnaVisina, telesnaTeza, telesnaTemperatura, sistolicniKrvniTlak, diastolicniKrvniTlak) {

	var podatki = {
		// Struktura predloge je na voljo na naslednjem spletnem naslovu:
		// https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		"ctx/language": "en",
		"ctx/territory": "SI",
		"ctx/time": datumInUra,
		"vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		"vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		"vital_signs/body_temperature/any_event/temperature|unit": "°C",
		"vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		"vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak
	    
	};
	var parametriZahteve = {
	    ehrId: ehrId,
	    templateId: 'Vital Signs',
	    format: 'FLAT'
	};
	$.ajax({
   url: baseUrl + "/composition?" + $.param(parametriZahteve),
   type: 'POST',
   contentType: 'application/json',
   data: JSON.stringify(podatki),
   headers: {
     "Authorization": getAuthorization()
   },
   success: function (res) {
   },
   error: function(err) {
   	console.log("Napaka!");
   }
	});
	
}

function preberiPodatkeOBolniku() {
	var ehrid = $("#inputEhrId").val();
	currentId = ehrid;
	preberiEHRodBolnika(ehrid);
}

function preberiEHRodBolnika(ehrid) {
	var ehrId = ehrid;

	$.ajax({
		url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
		type: 'GET',
		headers: {
     "Authorization": getAuthorization()
   },
 	success: function (data) {
  		var party = data.party;
  		$("#name").html("<b>Ime:</b> " + party.firstNames);
  		$("#surname").html("<b>Priimek:</b> " + party.lastNames);
  		$("#date").html("<b>Datum rojstva:</b> " + party.dateOfBirth);

  	},
  	error: function(err) {
  		console.log("Napaka!");
  	}
	});
}

var date = [];
var sistolicni = [];
var diastolicni = [];
var teza = [];
var visina = [];

function meritveVTabelo() {
	//Ko ponovno kliknemo na preberi meritve vitalnih znakov, zelimo da se bolj tocni podatki o meritvi (info cards) pocistijo
	$("#teza").html("");
	$("#tezaEnota").html("");
	$("#visina").html("");
	$("#visinaEnota").html("");
	$("#dys").html("");
	$("#dysEnota").html("");
	$("#sys").html("");
	$("#sysEnota").html("");
	
	$("#panelBmi").html("");
  	$("#panelKrvni").html("");
  	$("#dateOfMeasure").html("");
	
	
	if(currentId == undefined) {
		$("#obvId").html("Najprej morate izbrati pacienta!");
	} else {
		$("#obvId").html("");
		$("#dateAndTime").html("");
		
		//pridobimo podatke o datumih vnesenih vitalnih znakov, zraven tudi za telesno težo ter ostale parametre
		preberiMeritveVitalnihZnakov(currentId, function() {
			//Ko kliknemo na datum v tabeli, se nam prikazejo podrobnosti
			$('#vitalniZnaki tr td').click(function() {
				pokaziPodrobnosti($(this));
			});
			
			$("#scrol").get(0).scrollIntoView();
			
			//Ko preberemo meritve, te na graf tudi narisemo
			izrisiGraf();
			
		});
	
	}
}

function desnoPodatkiPocisti() {
	$("#teza").html("");
	$("#tezaEnota").html("");
	$("#visina").html("");
	$("#visinaEnota").html("");
	$("#dys").html("");
	$("#dysEnota").html("");
	$("#sys").html("");
	$("#sysEnota").html("");
}

/** Ko kliknemo na enega od datumov v tabeli pregled meritev vitalnih znakov
 *  oz. ko kliknemo na eno izmed meritev na grafu
 *  se nam na desni strani prikazejo podrobnosti tiste meritve
 */
var currentTeza;
var currentVis;
var currentSis;
var currentDis;
 
function pokaziPodrobnosti(element, tabIdx){
	if(element != undefined) {
		tabIdx = element.attr("value");
	}
	
	$("#teza").html("<b>" + teza[tabIdx].value + "</b>");
	$("#tezaEnota").html("<b>" + teza[tabIdx].enota + "</b>");
	$("#visina").html("<b>" +visina[tabIdx].value + "</b>");
	$("#visinaEnota").html("<b>" +visina[tabIdx].enota + "</b>");
	$("#dys").html("<b>" +diastolicni[tabIdx].value + "</b>");
	$("#dysEnota").html("<b>" +diastolicni[tabIdx].enota + "</b>");
	$("#sys").html("<b>" +sistolicni[tabIdx].value + "</b>");
	$("#sysEnota").html("<b>" +sistolicni[tabIdx].enota + "</b>");
	$("#dateOfMeasure").html("<b>" +date[tabIdx] + "</b>");
	
	currentTeza = teza[tabIdx].value;
	currentVis = visina[tabIdx].value;
	currentSis = sistolicni[tabIdx].value;
	currentDis = diastolicni[tabIdx].value;
	
	podajUgotovitve();
	
}

/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura in telesna teža).
 */
function preberiMeritveVitalnihZnakov(ehrId, callback) {
	
	var vis = false;
	var tez =  false;
	var blood = false;
	
	$.ajax({
		url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    	type: 'GET',
    	headers: {
       "Authorization": getAuthorization()
     },
    	success: function (data) {
  			var party = data.party;

  			$.ajax({
 		    url: baseUrl + "/view/" + ehrId + "/" + "weight",
 		    type: 'GET',
 		    headers: {
          "Authorization": getAuthorization()
      	},
 		    success: function (res) {
 		    	var tTeza = [];
 		    	if (res.length > 0) {
  		        for (var i in res) {
 	         		tTeza[i] = {value: res[i].weight, enota: res[i].unit};
  		        }
  		        
 		    	} else {
					console.log("Ni zapisa!");
 		    	}
 		    	var j = 0;
 		    	for(var i = tTeza.length - 1; i >= 0; i--) {
  		        	teza[j] = tTeza[i];
  		        	j++;
  		       }
 		    	tez = true;
 		    	if(vis && blood) {
 		    		callback();
 		    	}
 		    },
 		    error: function() {
 		    	console.log("Napaka pri branju telesne teže!");
 		    }
  			});
  				
  			$.ajax({
 		    url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
 		    type: 'GET',
 		    headers: {
          "Authorization": getAuthorization()
        },
 		    success: function (res) {
 		    	if (res.length > 0) {
  		        var tDiastolicni = [];
  		        var tSistolicni = [];
  		        for (var i in res) {
  		        		tDiastolicni[i] = {value: res[i].diastolic, enota: res[i].unit};
  		        		tSistolicni[i] = {value: res[i].systolic, enota: res[i].unit};
  		        }
  		        var j = 0;
  		        for(var i = tDiastolicni.length - 1; i >= 0; i--) {
  		        	diastolicni[j] = tDiastolicni[i];
  		        	sistolicni[j] = (tSistolicni[i]);
  		        	j++;
  		        }
  		        //Ko se prebere zadnja meritev
  		        //krvnega tlaka, lahko prikazemo graf
  		        blood = true;
  		        if(vis && tez) {
  		        	callback();
  		        }
  		        
 		    	} else {
					console.log("Ni zapisa!");
 		    	}
 		    	
 		    },
 		    error: function() {
 		    	console.log("Napaka pri branju krvnega tlaka!");
 		    }
  			});
  			
  			$.ajax({
 			  url: baseUrl + "/view/" + ehrId + "/" + "height",
 		    type: 'GET',
 		    headers: {
          "Authorization": getAuthorization()
      	},
 		    success: function (res) {
 		    	if (res.length > 0) {
 		    		var tDate = [];
 		    		var results;
 		    		var tVisina = [];
  		        for (var i in res) {
						tDate[i] = res[i].time;
						tVisina[i] = {value: res[i].height, enota:res[i].unit};
                
  		        }
  		        var j = 0;
  		        for(var i = tDate.length - 1; i >= 0; i--) {
  		        	date[j] = (tDate[i]);
  		        	visina[j] = (tVisina[i]);
  		        	j++;
  		        }
  		        
  		        for(var i in date) {
  		        	results += "<tr><td value='" + i + "'>" + date[i] + "</td></tr>";
  		        }
  		        $("#dateAndTime").append(results);
  		        
  		        vis = true;
  		        if(tez && blood) {
  		        	callback();
  		        }

 		    	} else {
 		    		console.log("Ni vpisov");
 		    	}
 		    	
 		    	
 		    },
 		    error: function() {
 		    	
 		    }
  			});
  				

    	},
    	error: function(err) {
    		console.log("Vpis s tem ID-jem ne obstaja!");
    	}
    	
	});

	
	
}

/** Risanje grafov
 * s Char.js
 */
 var krvniTlakChart;
  var bmiChart;
 
function izrisiGraf() {
	var dataSis = [];
	var dataDia = [];
	for(var i in sistolicni) {
		dataSis[i] = sistolicni[i].value;
		dataDia[i] = diastolicni[i].value;
	}
	
	krvniTlakChart = new Chart(document.getElementById("chartPritisk"), {
    type: 'line',
    data: {
      labels: date,
      datasets: [{ 
          data: dataSis,
          label: "Sistolični tlak",
          borderColor: "#3e95cd",
          fill: false
        }, { 
          data: dataDia,
          label: "Diastolični tlak",
          borderColor: "#8e5ea2",
          fill: false
        }
      ]
    },
    options: {
      title: {
        display: true
      }
    }
  });
  
  var dataBmi = [];
  for(var i in teza) {
  	dataBmi[i] = teza[i].value / (visina[i].value*visina[i].value/10000);
  }
  
  bmiChart = new Chart(document.getElementById("chartBMI"), {
    type: 'line',
    data: {
      labels: date,
      datasets: [{ 
          data: dataBmi,
          label: "IBM",
          borderColor: "#3e95cd",
          fill: false
        }
      ]
    },
    options: {
      title: {
        display: true
      }
     
    }
  });
  
  $("#chartBMI").click(function (evt) {
		var point = bmiChart.getElementAtEvent(evt)[0];
		var idx = point._index;
		pokaziPodrobnosti(undefined, idx);
		
	});
	
	$("#chartPritisk").click(function (evt) {
		var point = krvniTlakChart.getElementAtEvent(evt)[0];
		var idx = point._index;
		pokaziPodrobnosti(undefined, idx);
		
	});
  
}

/** Glede na datum izbrane meritve podamo ugotovitve
 *  Če je BMI previsok oz.
 *  krvni tlak previsok
 */
 var bolezniDebelost = ["Obesity", "Blood pressure", "Metabolic syndrome", "Heart disease", "Stroke", "Cancer", "Gallbladder disease", "Infertility",
 "Erectile dysfunction", "Non-alcoholic fatty liver disease", "Osteoarthritis"];
 
 var heartProblems = ['Hypertension',
'Coronary artery disease',
'Left ventricular hypertrophy',
'Heart failure',
'Transient ischemic attack',
'Stroke',
'Dementia',
'Mild cognitive impairment',
'Kidney failure',
'Glomerulosclerosis',
'Retinopathy',
'Optic neuropathy',
'Sexual dysfunction',
'Osteoporosis',
'Insomnia',
'Aortic dissection'];
 
function podajUgotovitve() {
	$("#panelBmi").html("<h4><b id='bmiTag'></b></h4> \
          <h4><b>Težave povezane s prekomerno telesno težo:</b></h4> \
          <div id='accordion'> \
            <div class='card' id='bmiProblems'> \
            </div> \
          </div>");
          
	$("#panelKrvni").html("<h4><b id='heartTag'></b></h4> \
          <h4><b>Težave povezane z zvišanim krvnim tlakom:</b></h4> \
          <div id='accordion'> \
            <div class='card' id='heartProblems'> \
            </div> \
          </div>");
	
	//1. Izracunamo BMI
	var bmi = currentTeza / (currentVis*currentVis/10000);
	bmi = bmi*100;
	bmi = Math.round(bmi);
	bmi /= 100;
	
	if(bmi < 25) {
		//BMI = 25: normalna telesna teža
		$("#bmiTag").html("BMI znaša " + bmi + " kg: normalna telesna teža");
	} else if(24 <= bmi && bmi <= 29) {
		//povišana telesna teža
		$("#bmiTag").html("BMI znaša " + bmi + ": povišana telesna teža");
	} else {
		//debelost
		$("#bmiTag").html("BMI znaša " + bmi + ": debelost");
	}
	
	$("#bmiProblems").html("");
	$("#heartProblems").html("");
	if(bmi >= 25) {
		//Prikazemo bolezni povezane s prekomerno telesno težo
		for(var i in bolezniDebelost) {
			pridobiWiki(bolezniDebelost[i], i, "bmiProblems");
		}
	} else {
		//Zdravi ste kot riba, nimate problemov z prekomerno težo.
		$("#bmiProblems").html(" <h4>Zdravi ste kot riba, nimate problemov z prekomerno težo.</h4>");
	}
	
	if(currentSis >= 140 && currentDis >= 90) {
		$("#heartTag").html("Krvni tlak znaša " + currentSis + "/" + currentDis + " mm[hg]: povišan krvni tlak");
		
		//Prikazemo probleme povezane s visokim krvnim tlakom
		for(var i in heartProblems) {
			pridobiWiki(heartProblems[i], i, "heartProblems");
		}
		
	} else {
		$("#heartTag").html("Krvni tlak znaša " + currentSis + "/" + currentDis + " mm[hg]: normalni krvni tlak");
		$("#heartProblems").html(" <h4>Zdravi ste kot riba, nimate problemov s previsokim krvnim tlakom.</h4>");
	}
	
}

function pridobiWiki(par, id, tip) {
	
	$.ajax({
		url: "https://en.wikipedia.org/api/rest_v1/page/summary/" + par,
		type: "GET",
 	success: function (response) {
  		
  		$("#" + tip).append(" \
              <div class='card-header'> \
                <h5 class='mb-0'> \
                  <button class='btn' data-toggle='collapse' data-target='#collapse" + tip + "" + id + "'> \
                    " + par + "\
                  </button> \
                </h5> \
              </div> \
              \
              <div id='collapse" + tip + "" +id + "' class='collapse'> \
                <div class='card-body'> \
                  " + response.extract + "\
                </div> \
              </div>");

  	},
  	error: function(err) {
  		console.log("Napaka!");
  	}
	});
	
}


 
 



